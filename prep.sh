#!/bin/bash
echo "preparing..."
echo "export CHV_HOME=`pwd`" > $HOME/.chvrc
echo "source ~/.chvrc" >> $HOME/.zshrc
echo "source ~/.chvrc" >> $HOME/.bashrc
echo "source ~/.chvrc" >> $HOME/.zprofile
echo "source ~/.chvrc" >> $HOME/.bash_profile
echo "source ~/.chvrc" >> $HOME/.profile
mkdir -p /tmp/chv/mnt
echo "done. mount your chv in /tmp/chv/mnt."
